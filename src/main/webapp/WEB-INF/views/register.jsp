<%-- 
    Document   : register
    Created on : Sep 13, 2018, 7:44:44 PM
    Author     : Nikanoras
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <form method="POST">
            <input placeholder="username" name="username">
            <input placeholder="password" name="password">
            <input type="hidden"
                   name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
            <input type="submit" value="Submit">
        </form>
    </body>
</html>
