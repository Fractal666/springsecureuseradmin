/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import lt.bit.beans.Users;
import lt.bit.dao.UsersDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nikanoras
 */
@Controller
//@RequestMapping("/")
public class HomeController {

    @Autowired
    private UsersDAO usersDao;

    @Autowired
    private PasswordEncoder encoder;

    @GetMapping
    public String home(Model model) {
        model.addAttribute("users", usersDao.findAll());
        return "home";
    }

    @GetMapping("/register")
    public String secure() {
        return "register";
    }

    @PostMapping("/register")
    public String register(@RequestParam String username,
            @RequestParam String password) {
        Users user = new Users();
        user.setUsername(username);
        user.setPassword(encoder.encode(password));
        usersDao.save(user);
        return "redirect:/";
    }

    @GetMapping("/secure")
    public String securePage() {
        return "secure";
    }

    @RequestMapping("logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        return "redirect:/";
    }
}
